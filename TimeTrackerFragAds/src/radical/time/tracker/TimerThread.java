package radical.time.tracker;

import android.app.Activity;

public class TimerThread extends Thread {

	TimeTracker timeTracker;

	public TimerThread(TimeTracker time) {
		// set instance of time tracker
		timeTracker = time;
	}

	@Override
	public void run() {
		while (timeTracker.timerRunning) {
			Activity act = (Activity) timeTracker.view.getContext();
			act.runOnUiThread(new Runnable() {

				@Override
				public void run() {
					timeTracker.updateTimerTextView();
				}

			});

			if (timeTracker.timerRunning)
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
	}
}
