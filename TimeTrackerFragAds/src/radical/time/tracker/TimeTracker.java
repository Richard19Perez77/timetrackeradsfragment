package radical.time.tracker;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.TransitionDrawable;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class TimeTracker {

	public TextView sText;

	/**
	 * The user's records are printed out in detail in this view.
	 */
	WebView webview;

	/**
	 * Tracking action buttons.
	 */
	public Button addRecordButton, delAllButton, delRecordButton,
			subRecordButton;

	/**
	 * The slot buttons are different instances of time tracking.
	 */
	Button slot1Button, slot2Button;

	/**
	 * Allow for the user to give input when performing actions such as delete
	 * that may have consequences.
	 */
	AlertDialog.Builder alert;

	/**
	 * Creates a small window alert for notifications.
	 */
	Toast myToast;

	/**
	 * Used to call timer actions
	 */
	Button timerButton;

	/**
	 * Displays the current timer increment
	 */
	public TextView timerText;

	/**
	 * Static variables for database transactions
	 */
	static final String DATABASE_NAME = "timetrackerdatabase.db";
	static final String TIME_TRACKER_TABLE = "time_records";
	static final String COLUMN_ID = "ID";
	static final String COLUMN_DAYS = "DAYS";
	static final String COLUMN_HOURS = "HOURS";
	static final String COLUMN_MINUTES = "MINUTES";
	static final String COLUMN_SECONDS = "SECONDS";
	static final String COLUMN_COMMENT = "COMMENT";
	static final String COLUMN_NAME = "NAME";
	static final String COLUMN_SLOT = "SLOT";

	static final String DEFAULT_SLOT_ONE = "Tracking 1";
	static final String DEFAULT_SLOT_TWO = "Tracking 2";

	/**
	 * Used to pull field values and perform time calculations.
	 */
	public int days;

	public Long daysLong;
	public BigInteger daysBigInteger;

	public int hours;

	public int minutes;

	public int seconds;

	public int currentSlot;

	int previousSlot;

	int records;

	String dayString, hourString, minuteString, secondString;

	public String slot1Name, slot2Name, currentSlotName;

	SQLiteDatabase scoreDB;

	EditText renameInput;

	/**
	 * The transition is the animation when pressing a button or on release of a
	 * button press.
	 */
	TransitionDrawable trans1, trans2, trans3;
	TransitionDrawable transa, transb, transc, transd, timerTrans;

	boolean timerRunning;

	Date start, stop;

	SharedPreferences sharedPreferences;

	public int timerSecond;

	public int timerMinute;

	public int timerHour;

	public int timerDay;

	public TextView mText;

	public TextView hText;

	public TextView dText;

	private String timerTextString;

	private TimerThread timerThread;

	final static int TRANSITION_DURATION = 500;

	public final static String MY_PREFERENCES = "MyPrefs";

	public final static String START_DATE = "startdate";
	public final static String TIMER_RUNNING = "timerrunning";

	// TextClock textClock;
	View view;

	public String cleanInputString(String currentSlotName) {
		// remove returns in string
		currentSlotName = currentSlotName.replaceAll("[\n\r]", "");
		// remove excess white space
		currentSlotName = currentSlotName.trim().replaceAll("\\s+", " ");
		return currentSlotName;
	}

	public void getTimeFromTextFields() {
		try {
			days = Integer.parseInt(dText.getText().toString());
		} catch (NumberFormatException nfe1) {
			days = 0;
		}

		try {
			hours = Integer.parseInt(hText.getText().toString());
		} catch (NumberFormatException nfe1) {
			hours = 0;
		}

		try {
			minutes = Integer.parseInt(mText.getText().toString());
		} catch (NumberFormatException nfe1) {
			minutes = 0;
		}

		try {
			seconds = Integer.parseInt(sText.getText().toString());
		} catch (NumberFormatException nfe1) {
			seconds = 0;
		}

	}

	public void storeSubtractTime() {
		switch (currentSlot) {
		case 1:
			currentSlotName = slot1Name;
			break;
		case 2:
			currentSlotName = slot2Name;
			break;
		}

		if (days != 0 || hours != 0 || minutes != 0 || seconds != 0) {
			// add by subtraction this means in the data base the
			// subtraction records are all negative values for each
			// field
			days *= -1;
			hours *= -1;
			minutes *= -1;
			seconds *= -1;

			ContentValues values = new ContentValues();
			values.put(COLUMN_SLOT, currentSlot);
			values.put(COLUMN_DAYS, days);
			values.put(COLUMN_HOURS, hours);
			values.put(COLUMN_MINUTES, minutes);
			values.put(COLUMN_SECONDS, seconds);
			values.put(COLUMN_NAME, currentSlotName);
			scoreDB.insert(TIME_TRACKER_TABLE, null, values);

			// set back to all positive numbers on screen so they appear
			// as they should
			days *= -1;
			hours *= -1;
			minutes *= -1;
			seconds *= -1;

			if (days != 0)
				dText.setText(String.valueOf(days));
			else
				dText.setText("");

			if (hours != 0)
				hText.setText(String.valueOf(hours));
			else
				hText.setText("");

			if (minutes != 0)
				mText.setText(String.valueOf(minutes));
			else
				mText.setText("");

			if (seconds != 0)
				sText.setText(String.valueOf(seconds));
			else
				sText.setText("");

		} else {
			// give toast message to enter a number
			String text = "Please enter an hour, minute or second.";
			showToast(text);
		}

	}

	public void storeAddTime() {
		switch (currentSlot) {
		case 1:
			currentSlotName = slot1Name;
			break;
		case 2:
			currentSlotName = slot2Name;
			break;
		}

		// there needs to be at lease one value to use
		if (days != 0 || hours != 0 || minutes != 0 || seconds != 0) {
			ContentValues values = new ContentValues();
			values.put(COLUMN_SLOT, currentSlot);
			values.put(COLUMN_DAYS, days);
			values.put(COLUMN_HOURS, hours);
			values.put(COLUMN_MINUTES, minutes);
			values.put(COLUMN_SECONDS, seconds);
			values.put(COLUMN_NAME, currentSlotName);
			scoreDB.insert(TIME_TRACKER_TABLE, null, values);
		} else {
			// give toast message to enter a number
			String text = "Please enter a day, hour, minute or second.";
			showToast(text);
		}

		if (days != 0)
			dText.setText(String.valueOf(days));
		else
			dText.setText("");

		if (hours != 0)
			hText.setText(String.valueOf(hours));
		else
			hText.setText("");

		if (minutes != 0)
			mText.setText(String.valueOf(minutes));
		else
			mText.setText("");

		if (seconds != 0)
			sText.setText(String.valueOf(seconds));
		else
			sText.setText("");
	}

	protected void setRecordCount() {
		// sets the count of records from current slot
		Cursor c = scoreDB.query(TIME_TRACKER_TABLE, new String[] { "*" },
				COLUMN_SLOT + "=" + currentSlot, null, null, null, null, null);
		records = c.getCount();
		c.close();
	}

	public void showToast(String message) {
		if (myToast != null)
			myToast.cancel();
		myToast = Toast
				.makeText(view.getContext(), message, Toast.LENGTH_SHORT);
		myToast.setText(message);
		myToast.show();
	}

	protected void switchPrevButtonTransDraw() {
		switch (previousSlot) {
		case 1:
			trans1.reverseTransition(TRANSITION_DURATION);
			break;
		case 2:
			trans2.reverseTransition(TRANSITION_DURATION);
			break;
		case 3:
			trans3.reverseTransition(TRANSITION_DURATION);
			break;
		}
		// only one transition check needed at a time
		previousSlot = currentSlot;
	}

	public void straightenTime() {
		// distribute over or under seconds and minutes

		if (seconds >= 60 || seconds <= -60) {
			minutes = seconds / 60;
			seconds = seconds % 60;
		}

		if (minutes >= 60 || minutes <= -60) {
			hours = minutes / 60;
			minutes = minutes % 60;
		}

		if (hours >= 24 || hours <= -24) {
			days = hours / 24;
			hours = hours % 24;
		}
	}

	public void reNameSlot() {
		ContentValues args = new ContentValues();
		if (currentSlotName.length() >= 10)
			currentSlotName = currentSlotName.substring(0, 10);
		args.put(COLUMN_NAME, currentSlotName);
		scoreDB.update(TIME_TRACKER_TABLE, args, COLUMN_SLOT + "="
				+ currentSlot, null);
	}

	public void resetCurrentSlotName() {
		// update current slot button name
		switch (currentSlot) {
		case 1:
			currentSlotName = slot1Name = DEFAULT_SLOT_ONE;
			slot1Button.setText(slot1Name);
			break;
		case 2:
			currentSlotName = slot2Name = DEFAULT_SLOT_TWO;
			slot2Button.setText(slot2Name);
			break;
		}
	}

	public void deleteLastRecord() {
		// gets the last record and removes it
		Cursor c = scoreDB.query(TIME_TRACKER_TABLE, new String[] { "*" },
				COLUMN_SLOT + "=" + currentSlot, null, null, null, null, null);
		// c.moveToFirst is the first record in the list
		c.moveToLast();
		if (c.getCount() > 0)
			scoreDB.delete(TIME_TRACKER_TABLE,
					COLUMN_ID + "=" + c.getString(0), null);
		c.close();
	}

	protected void delRecords() {
		days = hours = minutes = seconds = 0;
		dText.setText("");
		hText.setText("");
		mText.setText("");
		sText.setText("");
		resetCurrentSlotName();
		scoreDB.delete(TIME_TRACKER_TABLE, COLUMN_SLOT + "=" + currentSlot,
				null);
	}

	public int updateWebView() {
		// webview.clearView();
		webview.loadUrl("about:blank");

		Cursor cursor = scoreDB.query(TIME_TRACKER_TABLE, new String[] { "*" },
				COLUMN_SLOT + "=" + currentSlot, null, null, null, null, null);

		StringBuilder builder = new StringBuilder();
		builder.append("<table width='100%25'>");
		builder.append("<tr><td><h4>");
		builder.append("<FONT COLOR=00CC00># Name</FONT>");
		builder.append("</h4></td>");
		builder.append("<td><h4>");
		builder.append("<FONT COLOR=00CC00>Days</FONT>");
		builder.append("</h4></td>");
		builder.append("<td><h4>");
		builder.append("<FONT COLOR=00CC00>Hrs.</FONT>");
		builder.append("</h4></td>");
		builder.append("<td><h4>");
		builder.append("<FONT COLOR=00CC00>Min.</FONT>");
		builder.append("</h4></td>");
		builder.append("<td><h4>");
		builder.append("<FONT COLOR=00CC00>Sec.</FONT>");
		builder.append("</h4></td></tr>");

		cursor.moveToLast();

		days = hours = minutes = seconds = 0;

		int acc = 0;
		for (int i = cursor.getCount() - 1; i >= 0; i--) {
			builder.append("<tr>");
			builder.append("<td><h3><FONT COLOR=FFFFFF>"
					+ (cursor.getCount() - acc) + " " + cursor.getString(7)
					+ "</FONT></h3></td>");
			builder.append("<td><h3><FONT COLOR=FFFFFF>");
			days += Integer.parseInt(cursor.getString(2));
			builder.append(cursor.getString(2));
			builder.append("</FONT></h3></td>");
			builder.append("<td><h3><FONT COLOR=FFFFFF>");
			hours += Integer.parseInt(cursor.getString(3));
			builder.append(cursor.getString(3));
			builder.append("</FONT></h3></td>");
			builder.append("<td><h3><FONT COLOR=FFFFFF>");
			minutes += Integer.parseInt(cursor.getString(4));
			builder.append(cursor.getString(4));
			builder.append("</FONT></h3></td>");
			builder.append("<td><h3><FONT COLOR=FFFFFF>");
			seconds += Integer.parseInt(cursor.getString(5));
			builder.append(cursor.getString(5));
			builder.append("</FONT></h3></td>");
			builder.append("</tr>");
			acc++;
			cursor.moveToPrevious();
		}

		builder.append("</table></body></html>");

		cursor.close();

		straightenTime();

		builder.insert(
				0,
				"<html><body><h3><FONT COLOR=00CC00><center>Scrollable Records"
						+ "</center></FONT></h3><h4><FONT COLOR=00CC00><center>"
						+ currentSlotName + ": " + days + " days " + hours
						+ " hrs. " + minutes + " min. " + seconds
						+ " sec.</center></FONT></h4>");

		webview.loadData(builder.toString(), "text/html", "UTF-8");
		days = hours = minutes = seconds = 0;

		return cursor.getCount();
	}

	private void updateButtonNames() {
		// record an instance of each named slot
		Cursor c = scoreDB.query(TIME_TRACKER_TABLE, new String[] { "*" },
				null, null, null, null, null, null);
		c.moveToLast();
		ArrayList<Integer> numbers = new ArrayList<Integer>();

		for (int i = c.getCount() - 1; i >= 0; i--) {
			int sl = Integer.parseInt(c.getString(1));
			if (!numbers.contains(sl)) {
				numbers.add(sl);
				String nm = c.getString(7);
				setSlotName(sl, nm);
			}
			c.moveToPrevious();
		}

		c.close();

		switch (currentSlot) {
		case 1:
			currentSlotName = slot1Name;
			break;
		case 2:
			currentSlotName = slot2Name;
			break;
		}

	}

	private void setSlotName(int slotInt, String slotString) {
		switch (slotInt) {
		case 1:
			slot1Name = slotString;
			slot1Button.setText(slot1Name);
			break;
		case 2:
			slot2Name = slotString;
			slot2Button.setText(slot2Name);
			break;
		}
	}

	/**
	 * 
	 */
	public void settings() {
		// called on click of the setting option in the menu

	}

	public void removePhoneKeypad() {
		try {
			InputMethodManager inputManager = (InputMethodManager) view
					.getContext()
					.getSystemService(Context.INPUT_METHOD_SERVICE);

			inputManager.hideSoftInputFromWindow(((Activity) view.getContext())
					.getCurrentFocus().getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);
		} catch (Exception e) {
			// may be hidden already
		}
	}

	public void onResume() {
		scoreDB = view.getContext().openOrCreateDatabase(
				DATABASE_NAME,
				SQLiteDatabase.CREATE_IF_NECESSARY
						| SQLiteDatabase.OPEN_READWRITE, null);
		scoreDB.execSQL("CREATE TABLE IF NOT EXISTS " + TIME_TRACKER_TABLE
				+ " (" + COLUMN_ID + " INTEGER PRIMARY KEY, " + COLUMN_SLOT
				+ " INTEGER, " + COLUMN_DAYS + " INTEGER, " + COLUMN_HOURS
				+ " INTEGER, " + COLUMN_MINUTES + " INTEGER, " + COLUMN_SECONDS
				+ " INTEGER," + COLUMN_COMMENT + " VARCHAR," + COLUMN_NAME
				+ " VARCHAR) ");
		updateButtonNames();
		updateWebView();
	}

	public void startTimerThread() {
		if (timerThread == null
				|| timerThread.getState() == Thread.State.TERMINATED) {
			timerThread = new TimerThread(this);
			timerThread.start();
		}
	}

	public void timerStartStop() {
		// to start a timer
		if (!timerRunning) {
			// if not started create a new start time

			timerRunning = true;
			if (start == null) {
				start = new Date();
				showToast("Timer started");

				startTimerThread();

			} else {
				// add alert to start new date or leave it as previous
				alert = new AlertDialog.Builder(view.getContext());
				alert.setTitle("\uD83D\uDD50" + " TimeTracker");
				alert.setMessage("Continue current timer?");
				alert.setPositiveButton("Reset Timer",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// call method to parse values and add to edit
								// text fields
								start = new Date();
								showToast("Timer started");
								updateTimerTextView();
								startTimerThread();
							}
						});
				alert.setNegativeButton("Continue Timer",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// continue timer
								showToast("Timer continued");
								updateTimerTextView();
								startTimerThread();
							}
						});
				alert.show();
			}
		} else {
			timerRunning = false;
			stop = new Date();
			showToast("Timer stopped");
			updateTimerTextView();
		}
	}

	public void updateTimerTextView() {
		long startMillis = start.getTime();
		long endMillis = new Date().getTime();
		long timeSpan = endMillis - startMillis;

		long seconds = TimeUnit.SECONDS
				.convert(timeSpan, TimeUnit.MILLISECONDS);

		long minutes = seconds / 60;
		seconds = seconds % 60;

		long hours = minutes / 60;
		minutes = minutes % 60;

		long days = hours / 24;
		hours = hours % 60;

		timerSecond = (int) seconds;
		timerMinute = (int) minutes;
		timerHour = (int) hours;
		timerDay = (int) days;
		timerTextString = timerDay + "d." + timerHour + "h.\n" + timerMinute
				+ "m." + timerSecond + "s";
		timerText.setText(timerTextString);
	}

	public void sendTimerToInputFields() {
		// get difference of stop from start time and print values in edit texts
		dText.setText("" + timerDay);
		sText.setText("" + timerSecond);
		mText.setText("" + timerMinute);
		hText.setText("" + timerHour);

		// fill in hint for values above timer amount
		if (timerDay == 0) {
			dText.setText("");
			if (timerHour == 0) {
				hText.setText("");
				if (timerMinute == 0) {
					mText.setText("");
				}
			}
		}
	}

	public void onStart() {
		// TODO Auto-generated method stub

	}

	public void onRestart() {
		// TODO Auto-generated method stub

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// handle the menu options or return false
		return false;
	}

	public void killTimerThread() {
		if (timerThread != null && timerThread.isAlive()) {
			timerThread.interrupt();
			timerThread = null;
		}
	}

	public void onPause() {
		if (scoreDB.isOpen()) {
			scoreDB.close();
		}
		killTimerThread();
	}

	public void onStop() {
		Editor editor = sharedPreferences.edit();
		editor.putLong(START_DATE, start.getTime());
		editor.putBoolean(TIMER_RUNNING, timerRunning);
		editor.commit();
		killTimerThread();
	}

	public void onDestroy() {
		if (scoreDB.isOpen()) {
			scoreDB.close();
		}
		killTimerThread();
	}

	public void onCreate(View rootView) {
		view = rootView;

		sText = (TextView) rootView.findViewById(R.id.secondsInputText);
		mText = (TextView) rootView.findViewById(R.id.minutesInputText);
		hText = (TextView) rootView.findViewById(R.id.hoursInputText);
		dText = (TextView) rootView.findViewById(R.id.daysInputText);

		dText.setTextColor(Color.RED);
		hText.setTextColor(Color.RED);
		mText.setTextColor(Color.RED);
		sText.setTextColor(Color.RED);

		currentSlot = 1;
		previousSlot = 1;

		slot1Name = DEFAULT_SLOT_ONE;
		slot2Name = DEFAULT_SLOT_TWO;

		webview = (WebView) rootView.findViewById(R.id.webView1);
		webview.setBackgroundColor(0);

		timerText = (TextView) rootView.findViewById(R.id.timerText);
		timerText.setTextColor(Color.RED);

		timerButton = (Button) rootView.findViewById(R.id.timerButton);
		timerButton.setBackgroundResource(R.drawable.transitionbottombuttons);
		timerTrans = (TransitionDrawable) timerButton.getBackground();
		timerTrans.startTransition(TRANSITION_DURATION);
		timerButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				timerStartStop();
				timerTrans.startTransition(TRANSITION_DURATION);
			}
		});
		timerButton.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View v) {
				alert = new AlertDialog.Builder(view.getContext());
				alert.setTitle("Confirm");
				alert.setMessage("Send " + timerTextString
						+ " to input fields?");
				alert.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								// call method to parse values and add to edit
								// text fields
								sendTimerToInputFields();
								timerTrans.startTransition(TRANSITION_DURATION);
							}
						});
				alert.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
							}
						});
				alert.show();
				return false;
			}
		});

		// reference the delete button
		delAllButton = (Button) rootView.findViewById(R.id.delAllButton);

		// set the xml of the transition drawable
		delAllButton.setBackgroundResource(R.drawable.transitionbottombuttons);

		// reference the transition drawable to be manipulated
		transd = (TransitionDrawable) delAllButton.getBackground();
		transd.startTransition(TRANSITION_DURATION);
		delAllButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				alert = new AlertDialog.Builder(view.getContext());
				alert.setTitle("Confirm");
				alert.setMessage("Delete All Records for " + currentSlotName);
				alert.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								delRecords();
								updateWebView();
							}
						});
				alert.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
							}
						});
				alert.show();
			}
		});

		delRecordButton = (Button) rootView.findViewById(R.id.delRecordButton);
		delRecordButton
				.setBackgroundResource(R.drawable.transitionbottombuttons);

		transc = (TransitionDrawable) delRecordButton.getBackground();
		transc.startTransition(TRANSITION_DURATION);
		delRecordButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				setRecordCount();
				if (records > 0) {
					alert = new AlertDialog.Builder(view.getContext());
					alert.setTitle("Confirm");
					alert.setMessage("Delete Record #" + records + " for "
							+ currentSlotName);
					alert.setPositiveButton("Ok",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
									deleteLastRecord();
									updateWebView();
								}
							});
					alert.setNegativeButton("Cancel",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int whichButton) {
								}
							});
					alert.show();
				} else {
					// give toast message to enter a number
					String text = "No records in current slot to delete.";
					showToast(text);
				}
			}
		});

		addRecordButton = (Button) rootView.findViewById(R.id.addRecordButton);
		addRecordButton
				.setBackgroundResource(R.drawable.transitionbottombuttons);
		transa = (TransitionDrawable) addRecordButton.getBackground();
		transa.startTransition(TRANSITION_DURATION);
		addRecordButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				removePhoneKeypad();
				getTimeFromTextFields();
				straightenTime();
				storeAddTime();
				updateWebView();
				transa.startTransition(TRANSITION_DURATION);
			}
		});

		subRecordButton = (Button) rootView.findViewById(R.id.subRecordButton);
		subRecordButton
				.setBackgroundResource(R.drawable.transitionbottombuttons);
		transb = (TransitionDrawable) subRecordButton.getBackground();
		transb.startTransition(TRANSITION_DURATION);
		subRecordButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				removePhoneKeypad();
				getTimeFromTextFields();
				straightenTime();
				storeSubtractTime();
				updateWebView();
				transb.startTransition(TRANSITION_DURATION);
			}
		});

		slot1Button = (Button) rootView.findViewById(R.id.slotOneButton);
		slot1Button.setBackgroundResource(R.drawable.transitiontopbuttons);
		trans1 = (TransitionDrawable) slot1Button.getBackground();
		trans1.startTransition(TRANSITION_DURATION);
		slot1Button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				removePhoneKeypad();
				previousSlot = currentSlot;
				currentSlot = 1;
				currentSlotName = slot1Name;
				updateWebView();

				if (currentSlot != previousSlot) {
					switchPrevButtonTransDraw();
					trans1.startTransition(TRANSITION_DURATION);
				}
			}
		});

		slot1Button.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				alert = new AlertDialog.Builder(view.getContext());
				renameInput = new EditText(view.getContext());
				alert.setTitle(R.string.rename_string);
				alert.setMessage(R.string.type_new_and_ok);
				alert.setView(renameInput);
				alert.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								previousSlot = currentSlot;
								currentSlot = 1;

								currentSlotName = renameInput.getText()
										.toString();

								currentSlotName = cleanInputString(currentSlotName);

								slot1Name = currentSlotName;
								slot1Button.setText(currentSlotName);
								reNameSlot();
								if (currentSlot != previousSlot) {
									switchPrevButtonTransDraw();
									trans1.startTransition(TRANSITION_DURATION);
								}
								updateWebView();
							}

						});
				alert.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
							}
						});
				alert.show();
				return true;
			}
		});

		slot2Button = (Button) rootView.findViewById(R.id.slotTwoButton);
		slot2Button.setBackgroundResource(R.drawable.transitiontopbuttons);
		trans2 = (TransitionDrawable) slot2Button.getBackground();
		slot2Button.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				removePhoneKeypad();
				previousSlot = currentSlot;
				currentSlot = 2;
				currentSlotName = slot2Name;

				if (currentSlot != previousSlot) {
					switchPrevButtonTransDraw();
					trans2.startTransition(TRANSITION_DURATION);
				}
				updateWebView();
			}
		});
		slot2Button.setOnLongClickListener(new OnLongClickListener() {
			@Override
			public boolean onLongClick(View arg0) {
				alert = new AlertDialog.Builder(view.getContext());
				renameInput = new EditText(view.getContext());
				alert.setTitle(R.string.rename_string);
				alert.setMessage(R.string.type_new_and_ok);
				alert.setView(renameInput);
				alert.setPositiveButton("Ok",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								previousSlot = currentSlot;
								currentSlot = 2;
								currentSlotName = renameInput.getText()
										.toString();

								currentSlotName = cleanInputString(currentSlotName);

								slot2Name = currentSlotName;
								slot2Button.setText(currentSlotName);
								reNameSlot();
								if (currentSlot != previousSlot) {
									switchPrevButtonTransDraw();
									trans2.startTransition(TRANSITION_DURATION);
								}
								updateWebView();
							}
						});
				alert.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
							}
						});
				alert.show();
				return true;
			}
		});

		// check for previous start date to continue from
		sharedPreferences = view.getContext().getSharedPreferences(
				MY_PREFERENCES, Context.MODE_PRIVATE);

		if (sharedPreferences.contains(START_DATE)) {
			long tempstart = sharedPreferences.getLong(START_DATE, 0);
			Date tempdate = new Date(tempstart);
			start = tempdate;

			updateTimerTextView();
		}

		if (sharedPreferences.contains(TIMER_RUNNING)) {
			boolean running = sharedPreferences
					.getBoolean(TIMER_RUNNING, false);
			timerRunning = running;
			if (timerRunning) {
				timerThread = new TimerThread(this);
				timerThread.start();
			}

		}
	}
}