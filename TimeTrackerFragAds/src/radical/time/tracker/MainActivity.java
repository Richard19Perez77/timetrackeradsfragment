package radical.time.tracker;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my);
		Integer resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode == ConnectionResult.SUCCESS) {
			// Do what you want
		} else {
			Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode,
					this, 0);
			if (dialog != null) {
				// This dialog will help the user update to the latest
				// GooglePlayServices
				dialog.show();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
		}
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public TimeTracker timeTracker = new TimeTracker();

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_my, container,
					false);
			timeTracker.onCreate(rootView);
			return rootView;
		}

		@Override
		public void onConfigurationChanged(Configuration newConfig) {
			super.onConfigurationChanged(newConfig);
			// Checks the orientation of the screen
			if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
			}
		}

		@Override
		public void onResume() {
			super.onResume();
			timeTracker.onResume();
		}

		@Override
		public void onStop() {
			super.onStop();
			timeTracker.onStop();
		}

		@Override
		public void onDestroy() {
			super.onDestroy();
			timeTracker.onDestroy();
		}
	}

	public static class AdFragment extends Fragment {

		public AdView mAdView;

		@Override
		public void onActivityCreated(Bundle bundle) {
			super.onActivityCreated(bundle);
			mAdView = (AdView) getView().findViewById(R.id.adView);
			AdRequest adRequest = new AdRequest.Builder().build();
			mAdView.loadAd(adRequest);
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.fragment_ad, container, false);
		}

		@Override
		public void onResume() {
			super.onResume();

			// Resume the AdView.
			mAdView.resume();
		}

		@Override
		public void onPause() {
			// Pause the AdView.
			mAdView.pause();

			super.onPause();
		}

		@Override
		public void onDestroy() {
			// Destroy the AdView.
			mAdView.destroy();

			super.onDestroy();
		}
	}

}
